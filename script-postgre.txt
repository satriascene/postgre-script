Step 1:
create extension "uuid-ossp";

Step 2:
CREATE OR REPLACE FUNCTION "public"."uuid"()
  RETURNS "pg_catalog"."uuid" AS '$libdir/uuid-ossp', 'uuid_generate_v4'
  LANGUAGE c VOLATILE STRICT
  COST 1
  
Step 3:
select uuid();
